Vue.createApp({
    data() {
        return {
            tareas: [],
            valor: ''
        }
    },
    methods: {
        agregarTarea(){
            if(this.valor != ""){
                this.tareas.push(this.valor);
                this.valor = '';
            }
        }
    }
}).mount('#app');
const botonEle = document.querySelector('button');
const inputEle = document.querySelector('input');
const listaEle = document.querySelector('ul');


botonEle.addEventListener('click', () => {
    const valor = inputEle.value;
    const listaItem = document.createElement('li');
    listaItem.textContent = valor;
    listaEle.appendChild(listaItem);
    inputEle.value = '';
});